<?php
/**
 * @file
 * context_entity_type class
 *
 */

class contextEntityType extends context_condition {
  /**
   * Override of condition_values().
   */
  function condition_values() {
    $values = array();
    $entity_types = entity_get_info();
    foreach ($entity_types as $type => $entity_type) {
      $values[$type] = $entity_type['label'];
    }
    return $values;
  }

  /**
   * Override of options_form().
   */
  function options_form($context) {
    $defaults = $this->fetch_from_context($context, 'options');
    return array(
      'entity_type_form_reverse' => array(
        '#title' => t('Reverse conditions'),
        '#type' => 'checkbox',
        '#default_value' => isset($defaults['entity_type_form_reverse']) ? $defaults['entity_type_form_reverse'] : FALSE,
        '#description' =>  t('Reverse selected conditions'),
      ),
    );
  }

  /**
   * Override of execute().
   */
  function execute($type) {
    if ($this->condition_used()) {
      foreach ($this->get_contexts($type) as $context) {
        $entity_types = $this->fetch_from_context($context, 'values');
        $options = $this->fetch_from_context($context, 'options');
        if ($options['entity_type_form_reverse']) {
          if (!in_array($type, $entity_types)) {
            $this->condition_met($context, $type);
          }
        }
        else {
          if (in_array($type, $entity_types)) {
            $this->condition_met($context, $type);
          }
        }
      }
    }
  }
}
